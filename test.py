from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from time import sleep
from bs4 import BeautifulSoup
from lxml import html

import pandas as pd
import logging

import secrets

logging.root.setLevel(logging.NOTSET)
logger = logging.getLogger(__name__)

c_handler = logging.StreamHandler()
f_handler = logging.FileHandler("test_log.log")
ff_handler = logging.FileHandler("failed_test_log.log")
c_handler.setLevel(logging.ERROR)
f_handler.setLevel(logging.INFO)
ff_handler.setLevel(logging.WARNING)

f_format = logging.Formatter("%(name)s - %(levelname)s - %(message)s")
c_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)
ff_handler.setFormatter(f_format)

logger.addHandler(c_handler)
logger.addHandler(f_handler)
logger.addHandler(ff_handler)


class AobTest:
    def __init__(self, uname, password):
        self.username = uname
        self.password = password
        self.setup()
        self.login()
        self.test = 0

    def setup(self):
        self.driver = webdriver.Chrome(secrets.driverpath)
        self.driver.implicitly_wait(20)
        self.driver.maximize_window()
        self.driver.get(secrets.base_url)

    def login(self):
        self.driver.find_element_by_id("username").send_keys(self.username)
        pass_btn = self.driver.find_element_by_id("password")
        pass_btn.send_keys(self.password)
        pass_btn.send_keys(Keys.ENTER)

    def go_to_config(self):
        self.driver.find_element_by_link_text("Configuration").click()

    def go_to_innput(self):
        self.driver.find_element_by_link_text("Inputs").click()

    def clear_n_set_by_id(self, id, value):
        box = self.driver.find_element_by_id(id)
        box.clear()
        box.send_keys(value)

    def clear_filter(self):
        try:
            self.driver.find_element_by_css_selector(
                "#account-tab > div > div > div.table-caption.shared-tablecaption > div > div.control.shared-controls-textcontrol.shared-findinput.control-default > a"
            ).click()
            logger.info("Clear filter")
        except Exception as e:
            logger.info("Filter is already clear")

    def filter_box(self, search):
        self.driver.find_element_by_css_selector(
            "#account-tab > div > div > div.table-caption.shared-tablecaption > div > div.control.shared-controls-textcontrol.shared-findinput.control-default > input"
        ).send_keys(search)

    def cancel_btn(self):
        self.driver.find_element_by_css_selector(
            "body > div.dialog-placeholder > div > div > div > form > div.modal-footer > button"
        ).click()

    def add_btn(self):
        self.driver.find_element_by_css_selector(
            "body > div.dialog-placeholder > div > div > div > form > div.modal-footer > input"
        ).click()

    def add_inp_btn(self):
        self.driver.find_element_by_css_selector("#addInputBtn").click()

    def last_del_btn(self):
        self.driver.find_element_by_css_selector(
            "body > div.dialog-placeholder > div > div > div > form > div.modal-footer > input"
        ).click()

    def last_update_btn(self):
        self.driver.find_element_by_css_selector(
            "body > div.dialog-placeholder > div > div > div > form > div.modal-footer > input"
        ).click()

    def table_sort_btn(self):
        self.driver.find_element_by_link_text(secrets.acc_name).click()

    def handle_drop(self, name, value):
        try:
            search = "//*[@data-name='" + name + "']"
            box = self.driver.find_element_by_xpath(search)
            box.find_element_by_class_name("select2-choice").click()
            sleep(2)
            tr = self.driver.find_element_by_id("select2-drop")
            ser = tr.find_element_by_class_name("select2-search")
            ser.find_element_by_class_name("select2-input").send_keys(value)
            sleep(2)
            tr = self.driver.find_element_by_id("select2-drop")
            ser = tr.find_element_by_class_name("select2-results")
            sleep(2)
            ser.find_element_by_class_name("select2-result-label").click()

        except Exception as e:
            logger.error(str(e))
            logger.info("Error while selecting dropdown field {} ".format(name))

    def table_account_list(self):
        try:
            box = self.driver.find_element_by_css_selector(
                "#account-tab > div > div > div:nth-child(2)"
            )
            raw_html = box.get_attribute("innerHTML")
            df = pd.read_html(raw_html)[0]
            lst = list(df[secrets.acc_name])
            return lst

        except Exception as e:
            logger.error(str(e))
            logger.info("Error while checking table , test {} with ".format(self.test))
            return list()

    def get_error_msg(self):
        box = self.driver.find_element_by_css_selector(
            "body > div.dialog-placeholder > div > div > div > form > div.modal-body > div.msg.msg-error"
        )
        raw_html = box.get_attribute("innerHTML")
        soup = BeautifulSoup(raw_html, "html.parser")
        mydivs = soup.findAll("div", {"class": "msg-text"})
        text = ""
        for tag in mydivs:
            text += tag.text.strip()
        return text

    def check_table(self, name):
        try:
            box = self.driver.find_element_by_css_selector(
                "#account-tab > div > div > div:nth-child(2)"
            )
            raw_html = box.get_attribute("innerHTML")
            df = pd.read_html(raw_html)[0]
            lst = list(df[secrets.acc_name])

            for tab in lst:
                if tab == name:
                    return True
            return False

        except Exception as e:
            logger.error(str(e))
            logger.info("Error while checking table , test {} with ".format(self.test))
            return False

    def check_table_inp(self, name):
        try:
            box = self.driver.find_element_by_css_selector(
                "body > div.addonContainer > div > div:nth-child(4)"
            )
            raw_html = box.get_attribute("innerHTML")
            df = pd.read_html(raw_html)[0]
            lst = list(df[secrets.inp_table_name])

            for tab in lst:
                if tab == name:
                    return True
            return False

        except Exception as e:
            logger.error(str(e))
            logger.info("Error while checking table , test {} with ".format(self.test))
            return False

    def delete_account(self):
        try:
            self.driver.find_element_by_link_text("Action").click()
            self.driver.find_element_by_link_text("Delete").click()
            self.last_del_btn()
            logger.info("Account deleted sucessfully.")
            return True
        except Exception as e:
            logger.error(str(e))
            logger.info("Error while Deleting Account")
            return False

    def delete_account_by_filter(self, name):
        self.filter_box(name)
        sleep(5)
        while True:
            sleep(5)
            if not self.delete_account():
                break
        sleep(5)
        self.clear_filter()

    def delete_all_accounts(self):
        while True:
            sleep(5)
            if not self.delete_account():
                break
        sleep(5)

    def delete_account_by_name(self, name):
        try:
            selector = (
                "#account-tab > div > div > div:nth-child(2) > table > tbody > tr.apps-table-tablerow.even.row-"
                + name.strip()
                + " > td.actions.col-actions > a"
            )
            self.driver.find_element_by_css_selector(selector).click()
            self.driver.find_element_by_link_text("Delete").click()
            self.last_del_btn()
            logger.info("Account {} deleted sucessfully.".format(name))
        except Exception as e:
            try:
                selector = (
                    "#account-tab > div > div > div:nth-child(2) > table > tbody > tr.apps-table-tablerow.odd.row-"
                    + name.strip()
                    + " > td.actions.col-actions > a"
                )
                self.driver.find_element_by_css_selector(selector).click()
                self.driver.find_element_by_link_text("Delete").click()
                self.last_del_btn()
                logger.info("Account {} deleted sucessfully.".format(name))
            except Exception as e:
                logger.error(str(e))
                logger.info("Error while Deleting Account {}".format(name))

    def edit_btn(self, name):
        try:
            selector = (
                "#account-tab > div > div > div:nth-child(2) > table > tbody > tr.apps-table-tablerow.even.row-"
                + name.strip()
                + " > td.actions.col-actions > a"
            )
            self.driver.find_element_by_css_selector(selector).click()
            self.driver.find_element_by_link_text("Edit").click()
            return True
        except Exception as e:

            try:
                selector = (
                    "#account-tab > div > div > div:nth-child(2) > table > tbody > tr.apps-table-tablerow.odd.row-"
                    + name.strip()
                    + " > td.actions.col-actions > a"
                )
                self.driver.find_element_by_css_selector(selector).click()
                self.driver.find_element_by_link_text("Edit").click()
                return True
            except Exception as e:
                logger.error(str(e))
                logger.info("Error while Searching  Account {}".format(name))
                return False

    def edit_account(self, new_dict={}):
        sleep(3)
        logger.info("Start Editing Account  with {} parameters ".format(new_dict))

        name = new_dict[secrets.acc_id]
        del new_dict[secrets.acc_id]
        sleep(5)
        if self.edit_btn(name):
            for key, value in new_dict.items():
                self.clear_n_set_by_id(key, value)
            sleep(3)
            self.last_update_btn()
            sleep(15)
            try:
                self.cancel_btn()
                logger.info("Failed to edit Account with {} params".format(new_dict))

            except Exception as e:
                logger.info("Edited Account with {} params".format(new_dict))

    def select_input(self):
        pass

    def add_input(self, typ="", params={}, drop_dic={}, single_inp=False):

        self.go_to_innput()
        sleep(8)
        self.add_inp_btn()
        if not single_inp:
            try:
                self.driver.find_element_by_link_text(typ).click()

            except Exception as e:
                logger.error("No input exist with {} name".format(typ))

        for key, value in params.items():
            self.clear_n_set_by_id(key, value)
            sleep(1)

        for key, value in drop_dic.items():
            self.handle_drop(key, value)
            sleep(2)

        self.add_btn()
        sleep(7)

        try:
            self.cancel_btn()
            logger.info("Failed to create input {} ".format(typ))
            return False
        except Exception as e:

            idgf = typ.lower().replace(" ", "_")
            name = getattr(secrets, idgf)
            if self.check_table_inp(params[name]):
                logger.info("Added input {}".format(typ))
                return True
            else:
                logger.info("Failed to add input {} params".format(typ))
                return False

    def velidate_input(
        self, typ="", params={}, drop_dic={}, result=False, single_inp=False
    ):

        self.go_to_innput()
        sleep(6)
        self.add_inp_btn()

        if not single_inp:
            try:
                self.driver.find_element_by_link_text(typ).click()

            except Exception as e:
                logger.error("No input exist with {} name".format(typ))

        for key, value in params.items():
            self.clear_n_set_by_id(key, value)
            sleep(1)

        for key, value in drop_dic.items():
            self.handle_drop(key, value)
            sleep(2)

        self.add_btn()
        sleep(7)

        try:
            self.driver.find_element_by_css_selector(
                "body > div.dialog-placeholder > div > div > div > form > div.modal-body > div.msg.msg-error"
            )

        except Exception as e:

            sleep(20)
            idgf = typ.lower().replace(" ", "_")
            name = getattr(secrets, idgf)
            if result:
                if self.check_table_inp(params[name]):
                    logger.info(
                        "Test success for input {} with param {} ".format(typ, params)
                    )
                    return True
                else:
                    logger.info(
                        "Strange : No error got but input not found in table for input {} with param {} ".format(
                            typ, params
                        )
                    )
                    return False

            else:
                if self.check_table_inp(params[name]):
                    logger.info(
                        "Test Failed (expecting error) for input {} with param {} ".format(
                            typ, params
                        )
                    )
                    return False
                else:
                    logger.info(
                        "Strange : No error got but input not found in table for input {} with param {} ".format(
                            typ, params
                        )
                    )
                    return False

        else:
            err_msg = self.get_error_msg()
            if not result:

                self.cancel_btn()
                logger.info(
                    "Test passed for input {} with param {} ".format(typ, params)
                )
                return True

            else:
                self.cancel_btn()
                logger.info(
                    "Test Failed (Expected True but got error)for input {} with param {} ".format(
                        typ, params
                    )
                )
                return False

    def add_account(self, ids_dict={}):
        sleep(3)
        logger.info("Start Adding Account  with {} parameters ".format(ids_dict))

        try:
            self.driver.find_element_by_id("addAccountBtn").click()

        except Exception as e:
            logger.error(str(e))
            try:
                self.go_to_config()
                sleep(10)
                self.driver.find_element_by_id("addAccountBtn").click()

            except Exception as e:
                logger.error(str(e))
                logger.info("Failed Adding Account {}".format(ids_dict))
                return

        for key, value in ids_dict.items():
            self.clear_n_set_by_id(key, value)

        self.add_btn()
        sleep(15)

        try:
            self.cancel_btn()
            logger.info("Failed to create Account with {} params".format(ids_dict))
            return False
        except Exception as e:
            if self.check_table(ids_dict[secrets.acc_id]):
                logger.info("Added Account with {} params".format(ids_dict))
                return True
            else:
                logger.info("Failed to add Account with {} params".format(ids_dict))
                return False

    def validate_account(self, ids_dict={}, result=False):
        self.test += 1
        sleep(3)
        logger.info("Start test {}  with {} ".format(self.test, ids_dict))

        try:
            self.driver.find_element_by_id("addAccountBtn").click()

        except Exception as e:
            self.go_to_config()
            sleep(10)
            logger.error(str(e))
            try:
                self.driver.find_element_by_id("addAccountBtn").click()
            except Exception as e:
                logger.error(str(e))
                logger.info(
                    "Failed Executing test {} with {}".format(self.test, ids_dict)
                )
                return

        for key, value in ids_dict.items():
            self.clear_n_set_by_id(key, value)

        self.add_btn()
        sleep(50)

        try:
            self.driver.find_element_by_css_selector(
                "body > div.dialog-placeholder > div > div > div > form > div.modal-body > div.msg.msg-error"
            )
        except Exception as e:
            sleep(20)
            if result:
                if self.check_table(ids_dict[secrets.acc_id]):
                    logger.info("pass test {} with {}".format(self.test, ids_dict))
                    self.delete_account()

                else:
                    logger.info(
                        "pass test {} but name not found in table with {} maybe taking too much time".format(
                            self.test, ids_dict
                        )
                    )

            else:
                if self.check_table(ids_dict[secrets.acc_id]):
                    logger.warning(
                        "Failed Test {} with ids {} .Expected : Error but passed".format(
                            self.test, ids_dict
                        )
                    )
                else:
                    logger.warning(
                        "Failed Test {} not failed or passed  maybe taking too much time".format(
                            self.test, ids_dict
                        )
                    )

        else:
            err_msg = self.get_error_msg()
            if not result:
                self.cancel_btn()
                logger.info(
                    "pass test {} with {} and error massage {}".format(
                        self.test, ids_dict, err_msg
                    )
                )

            else:
                self.cancel_btn()
                logger.warning(
                    "Failed Test {} with ids {} .Expected : Passing but Failed with Error {}".format(
                        self.test, ids_dict, err_msg
                    )
                )


if __name__ == "__main__":
    temp_dic = {
        "account-name": "test",
        "account-username": "cresttestdata",
        "account-password": "****",
    }

    temp_par = {
        "storage_blob_data-name": "test440",
        "storage_blob_data-interval": 180,
        "storage_blob_data-container_name": "testabc",
        "storage_blob_data-blob_list": "myblob",
    }

    drop_dic = {"index": "main", "azure_storage_account": "test"}

    obj = AobTest(secrets.usernamee, secrets.passwordd)
    # obj.add_account(temp_dic)
    # sleep(10)
    # obj.add_input("MSCS Blob Data", temp_par, drop_dic)
