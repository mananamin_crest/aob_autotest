from time import sleep
import datetime
import itertools
import json
import os
import string
import random
import secrets
import test


def place_ones(size, count):
    for positions in itertools.combinations(range(size), count):
        p = [0] * size
        for i in positions:
            p[i] = 1
        yield p


def randomuname():
    chars = string.ascii_lowercase
    size = random.randint(8, 12)
    return "".join(random.choice(chars) for x in range(size))


def randompassword():
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    size = random.randint(8, 12)
    return "".join(random.choice(chars) for x in range(size))


def gen_test_req(attr, vals):
    temp_val = vals[0]
    fake_val = temp_val.copy()
    for key, value in fake_val.items():
        fake_val[key] = ""
    fak_list = list(fake_val.items())
    repeat = len(attr)

    req = []
    for idx, atr in enumerate(attr):
        assert atr["id"] == fak_list[idx][0], "Please check order in config.json"
        if atr["required"]:
            req.append(1)
        else:
            req.append(0)
    req_sum = sum(req)
    lst = list(itertools.product([0, 1], repeat=repeat))

    for val in vals:
        val_list = list(val.items())
        for value in lst:
            result = False

            if sum([x * y for x, y in zip(req, value)]) == req_sum:
                result = True

            final = {}
            for idx, check in enumerate(value):
                if check:
                    final[val_list[idx][0]] = val_list[idx][1]
                else:
                    final[fak_list[idx][0]] = fak_list[idx][1]

            yield (final, result)


def fake_value_gen(typ):
    result = []
    if typ == "number":
        result.append("number")
        result.append("123abc")

    elif typ == "password":
        result.append(randompassword())

    elif typ == "name":
        result.append("123")
        result.append("123abc")
    else:
        pass

    return result


def gen_type_test(attr, vals):
    val = vals[0]

    for idx, atr in enumerate(attr):

        fake_value = fake_value_gen(atr["type"])
        if fake_value:
            for fak in fake_value:
                final_fak = val.copy()
                final_fak[atr["id"]] = fak

                yield final_fak, False


def gen_test_req_fast(attr, vals):
    temp_val = vals[0]
    fake_val = temp_val.copy()
    for key, value in fake_val.items():
        fake_val[key] = ""

    fak_list = list(fake_val.items())

    repeat = len(attr)

    req = []
    for idx, atr in enumerate(attr):
        # print(atr["id"], " ", fak_list[idx][0])
        assert atr["id"] == fak_list[idx][0], "Please check order in config.json"

        if atr["required"]:
            req.append(1)
        else:
            req.append(0)
    req_sum = sum(req)

    assert repeat > 0, "No attributes found"
    lst = list(place_ones(repeat, repeat - 1))

    for val in vals:
        val_list = list(val.items())
        for value in lst:
            result = False

            if sum([x * y for x, y in zip(req, value)]) == req_sum:
                result = True

            final = {}
            for idx, check in enumerate(value):
                if check:
                    final[val_list[idx][0]] = val_list[idx][1]
                else:
                    final[fak_list[idx][0]] = fak_list[idx][1]

            yield (final, result)


def is_sorted(lst):
    lst = [name.lower() for name in lst]
    sort = all(lst[i] <= lst[i + 1] for i in range(len(lst) - 1))
    sort_rev = all(lst[i] >= lst[i + 1] for i in range(len(lst) - 1))
    return sort or sort_rev


def accounts_add(obj, attr, vals, count=5):
    names = []
    for _ in range(count):
        temp_val = vals[0].copy()
        name = randomuname()
        temp_val[secrets.acc_id] = name
        result = obj.add_account(temp_val)
        if result:
            names.append(name)
    return names


def accounts_delete(obj, lst):
    for table in lst:
        obj.delete_account_by_name(table)
        sleep(10)


def sort_btn_test(obj, attr, vals, count=5):
    names = accounts_add(obj, attr, vals, count=5)

    sleep(5)
    obj.table_sort_btn()
    sleep(15)
    table_list = obj.table_account_list()

    if is_sorted(table_list):
        print("Sort Test passed")
    else:
        print("Sort test Failed")
    accounts_delete(obj, names)


def req_test(obj, attr, vals):
    for id_dict, result in gen_test_req(attr, vals):
        print(id_dict, result)
        obj.validate_account(id_dict, result)


def type_test(obj, attr, vals):
    for id_dict, result in gen_type_test(attr, vals):
        print(id_dict, result)
        obj.validate_account(id_dict, result)


def filter_test(obj, attr, vals, count=2):
    names = accounts_add(obj, attr, vals, count=count)

    query = names[0]
    obj.filter_box(query)
    sleep(15)
    results = obj.table_account_list()

    flag = True
    for result in results:
        if result != query:
            flag = False

    if flag:
        print("Filter Test passed")
    else:
        print("Filter Test Failed")

    obj.clear_filter
    sleep(5)
    accounts_delete(obj, names)


def filter_n_sort_test(obj, attr, vals):
    names = accounts_add(obj, attr, vals, count=5)

    sleep(5)
    obj.table_sort_btn()
    sleep(15)
    table_list = obj.table_account_list()

    if is_sorted(table_list):
        print("Sort Test passed")
    else:
        print("Sort test Failed")

    sleep(10)
    query = names[0]
    obj.filter_box(query)
    sleep(15)
    results = obj.table_account_list()

    flag = True
    for result in results:
        if result != query:
            flag = False

    if flag:
        print("Filter Test passed")
    else:
        print("Filter Test Failed")
    obj.clear_filter
    sleep(5)

    accounts_delete(obj, names)


if __name__ == "__main__":

    file_path = secrets.conf_file_path
    config = None
    with open(file_path) as f:
        config = json.load(f)

    attr = config["config_page"]["attributes"]
    vals = config["config_page"]["values"]

    obj = test.AobTest(secrets.usernamee, secrets.passwordd)

    # req_test(obj, attr, vals)
    # type_test(obj, attr, vals)
    # sort_btn_test(obj, attr, vals)
    # filter_test(obj, attr, vals, count=2)
